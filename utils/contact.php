<?php
if (!$_POST) :
    return false;
else :
    $errors = [];
    $message = "Contact Form filled out by ";
    $message .= $_POST['cf-name']." (".$_POST['cf-email'].") on ";
    $message .= $_POST['cf-date-submitted'].". \n\r";

    if ($_POST['cf-institution']) $message .= "Affiliated Institution: ".$_POST['cf-institution']." \n\r";

    $message .= "Note: ".$_POST['cf-message']." \n\r";

    if ($_POST['cf-email']) :
        $to = "oasis-brains@nrg.wustl.edu";
        $subject = "OASIS Contact from ".$_POST['cf-name'];
        $from = $_POST['cf-email'];
        mail($to,$subject,$message,"From: ".$from);
        echo $message;
        return true;
    else :
        echo "Fail";
        return false;
    endif;
endif;
?>