<?php
if (!$_POST) :
    return false;
else :
    $errors = [];
    $message = "Data Use Terms for ".implode(" & ",$_POST['dut-dataset'])." agreed to by ";
    $message .= $_POST['dut-firstname']." ".$_POST['dut-lastname']." (".$_POST['dut-email'].") on ";
    $message .= $_POST['dut-date-submitted'].". \n\r";

    $message .= "Position(s): ".$_POST['dut-position'];
    if ($_POST['dut-other-position']) $message .= ", ".$_POST['dut-other-position'];
    $message .= "\n\r";

    $message .= "Affiliated Institution: ".$_POST['dut-institution']." \n\r";

    if ($_POST['dut-sector']) $message .= "Research Sector: ".implode(", ",$_POST['dut-sector']);
    if ($_POST['dut-other-sector']) $message .= ", ".$_POST['dut-other-sector'];
    $message .= "\n\r";

    $message .= "Proposed Research: ".$_POST['dut-purpose']."\n\r";

    if ($_POST['dut-adrc-agreement']) {
        $message .= "Knight ADRC ID: ".$_POST['dut-adrc-agreement-id'];
        $message .= "\n\r";
    }

    if ($_POST['dut-email']) :
        $to = "oasis-brains@nrg.wustl.edu";
        $subject = "Accepted OASIS DUT by ".$_POST['dut-firstname']." ".$_POST['dut-lastname'];
        $from = $_POST['dut-email'];
        mail($to,$subject,$message,"From: ".$from);
        echo $message;
        return true;
    else :
        echo "Fail";
        return false;
    endif;
endif;
?>