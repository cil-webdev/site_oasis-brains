<?php
require_once ($_SERVER['DOCUMENT_ROOT'].'/utils/db-login.php');

/* do stuff */
$dataset = implode(", ", $_POST['dut-dataset']);
$sector = "";
if ($_POST['dut-sector']) :
    foreach ($_POST['dut-sector'] as $value) :
        $sector .= ($sector) ? ", " . $value : $value;
    endforeach;
endif;

if ($_POST['dut-other-sector']) {
    $sector = ($sector) ? $sector . ", " . trim($_POST['dut-other-sector']) : trim($_POST['dut-other-sector']);
}

$position = $_POST['dut-position'];
if ($_POST['dut-other-position']) {
    $position = trim($_POST['dut-other-position']);
}

$research = trim( nl2br( htmlentities( $_POST['dut-purpose'], ENT_QUOTES, 'UTF-8' )));

$adrc = ($_POST['dut-adrc-agreement']) ? '1' : '0';


$q = "INSERT INTO applicants (firstname, lastname, email, dataset, version, research_position, institution, sector, research, knight_adrc, knight_adrc_id) ";
$q .= "VALUES ('".$_POST['dut-firstname']."', '".$_POST['dut-lastname']."', '".$_POST['dut-email']."', '".$dataset."', '".$_POST['dut-version']."', '".$position."', '".$_POST['dut-institution']."', '".$sector."', '".$research."', '".$adrc."', '".$_POST['dut-adrc-agreement-id']."');";
$r = mysqli_query ($db,$q) or die ($q);

/* finish doing stuff */

mysqli_close($db);
?>
