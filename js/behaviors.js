/**
 * Created by will on 10/16/17.
 */

(function() {
    'use strict';

    // parse page redirects that use '?' URL queries
    if (window.location.search.length > 0) {
        var hashtag = window.location.search.split('?')[1];
        if (hashtag === 'error') {
            $('#dut-modal-status').modal('show')
                .find('.modal-body').append('<p><i class="fa fa-warning"></i> Sorry, we could not find that content. Please <a href="#contact" onclick="javascript:window.hideModal(this)">contact us</a> using the form below.</p>')
        }
        else {
            window.location.assign('/#'+hashtag);
        }
    }

    window.hideModal = function(el){
        $(el).parents('.modal').modal('hide');
    };


    // topnav effect on scroll
    function toggleMastheadStyle(){
        var activeNav = $('.masthead').hasClass('masthead-visible');
        if ($(document).scrollTop() >= 220 && !activeNav) $('.masthead').addClass('masthead-visible');
        if ($(document).scrollTop() < 220 && activeNav) $('.masthead').removeClass('masthead-visible');
    }

    $(document).on('scroll',function(){
        toggleMastheadStyle();
    });

    $('#masthead-navbar-toggler').on('show.bs.collapse', function(){
        $(this).parents('.masthead').addClass('masthead-visible');
    });

    $('#masthead-navbar-toggler').on('hide.bs.collapse', function(){
        toggleMastheadStyle();
    });


    $('input[name=dut-position]').on('change',function(){
        var checked = $('#dut-other-position-toggle').is(':checked');
        (checked) ?
            $('#dut-other-position').removeClass('hidden') :
            $('#dut-other-position').addClass('hidden');
    });

    $('#dut-other-sector-toggle').on('change',function(){
        var checked = $(this).is(':checked');
        (checked) ?
            $('#dut-other-sector').removeClass('hidden') :
            $('#dut-other-sector').addClass('hidden');
    });

    function clearForm(form){
        $(form).find('input[type=text]').val('');
        $(form).find('textarea').val('');
        $(form).find('input[type=email]').val('');
        $(form).find('input[type=checkbox]').prop('checked',false);
        $(form).find('input[type=radio]').prop('checked',false);
    }

    // data use form validation
    $('#dut-modal').on('show.bs.modal',function(){
        var form = document.getElementById('dut-form');
        clearForm(form);
    });

    function addInvalidText($el,message){
        message = message || $el[0].validationMessage;
        $el.parent().find('.invalid').remove();
        $el.parent().append('' +
            '<small class="invalid form-text">' +
            message +
            '</small>');
    }

    /*
     * SUBMIT CONTACT FORM
     */
    window.validateContactForm = function(){
        // event.preventDefault();
        $('#cf-date-submitted').val(new Date().toLocaleString());

        var form = document.getElementById('contact-form');

        if (form.checkValidity() === false) {
            // event.preventDefault();
            // event.stopPropagation();
            $(form).find(':invalid').each(function () {
                addInvalidText($(this));
            });
        }
        else if ($(form).find('input[name=cf-username]').val().length) {
            // catch honeypot users
            $('#dut-modal-contact-thanks').modal('show');
        }
        else {
            // if form is valid
            $.ajax({
                type: 'POST',
                url: '/utils/contact.php',
                data: $(form).serialize(),
                success: function(data){
                    if (data.toLowerCase() !== "fail") {
                        $('#dut-modal-status').modal('show')
                            .find('.modal-body').append('<p><i class="fa fa-check"></i> Thanks for contacting the OASIS Brains project.');
                        clearForm(form);
                    }
                    else {
                        $('#dut-modal-status').modal('show')
                            .find('.modal-body').append('<p><i class="fa fa-warning"></i> An error occurred, and your contact form could not be submitted. Please contact oasis-brains@nrg.wustl.edu.</p>')
                    }
                },
                fail: function(e){
                    $('#dut-modal-status').modal('show')
                        .find('.modal-body').append('<p><i class="fa fa-warning"></i> An error occurred, and your contact form could not be submitted. Please contact oasis-brains@nrg.wustl.edu.</p>')
                        .find('.modal-body').append(e.statusText);
                }
            });
        }
    };

    /*
     * SUBMIT DUT ACCEPTANCE
     */
    window.validateDutForm = function(){
        $('#dut-date-submitted').val(new Date().toLocaleString());

        var form = document.getElementById('dut-form');
        var dataset = [], position = [], sector = [];
        $(form).find('input.dut-dataset:checked').each(function () {
            dataset.push($(this).val());
        });

        $(form).find('input.dut-position:checked').each(function(){
            position.push($(this).val());
        });

        $(form).find('input.dut-sector:checked').each(function(){
            sector.push($(this).val());
        });

        if (form.checkValidity() === false || !dataset.length || !position.length || !sector.length) {
            $(form).find(':invalid').each(function () {
                addInvalidText($(this));
            });

            if (!dataset.length) {
                addInvalidText($('#dut-dataset-group'),'Please Select A Dataset');
            }

            if (!position.length) {
                addInvalidText($('#dut-position-group'), 'Please Select A Position');
            }

            if (!sector.length) {
                addInvalidText($('#dut-sector-group'), 'Please Select A Sector');
            }

        }
        else if ($(form).find('input[name=dut-username]').val().length) {
            // catch honeypot users
            $('#dut-modal').modal('hide');
            $('#dut-modal-thanks').modal('show');
        }
        else {
            // if form is valid
            $.ajax({
                type: 'POST',
                url: '/utils/store-dut-acceptance.php',
                data: $(form).serialize(),
                success: function(){
                    console.log("Stored acceptance in SQL database");
                },
                fail: function(e){
                    console.log(e);
                }
            });

            $.ajax({
                type: 'POST',
                url: '/utils/dut-notification.php',
                data: $(form).serialize(),
                success: function(data){
                    $('#dut-modal').modal('hide');
                    if (data.toLowerCase() !== "fail") {
                        $('#dut-modal-thanks').modal('show');
                    }
                    else {
                        $('#dut-modal-status').modal('show')
                            .find('.modal-body').append('<p><i class="fa fa-warning"></i> An error occurred, and your DUT could not be submitted. Please contact oasis-brains@nrg.wustl.edu.</p>')
                    }
                },
                fail: function(e){
                    $('#dut-modal-status').modal('show')
                        .find('.modal-body').append('<p><i class="fa fa-warning"></i> An error occurred, and your DUT could not be submitted. Please contact oasis-brains@nrg.wustl.edu.</p>')
                        .find('.modal-body').append(e.statusText);
                }
            });
        }
        form.classList.add('was-validated');
    };

    $('input').on('focus',function(){
        $(this).parent().find('.invalid').remove();
    });
    $('input[type=checkbox]').on('click',function(){
        $(this).parents('.form-check').find('.invalid').remove();
    });

    $('input[type=radio]').on('click',function(){
        $(this).parents('.form-check').find('.invalid').remove();
    });
    $('select').on('focus', function(){
        $(this).parent().find('.invalid').remove();
    });
    $('textarea').on('focus',function(){
        $(this).parent().find('.invalid').remove();
    })
})();